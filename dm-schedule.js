import { LitElement } from 'lit-element';
import {getToken } from '@practitioner/security-module';
import '@cells-components/cells-generic-dp';

/**
This component es utilizado para comunicar a la API con los métodos de recordatorios.
Estos método necesitan autenticación.

Example:

```html
<dm-schedule></dm-schedule>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class DmSchedule extends LitElement {
  static get is() {
    return 'dm-schedule';
  }

  // Declare properties
  static get properties() {
    return {
      host: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = "http://localhost:1122";
  }

  /**
   * Retorna todos las programaciones para un determinado usuario.
   * Requiere autenticación.
   */
  getSchedules() {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');
    
    let dp = new DP();
    dp.headers = {'Authorization': 'JWT '+ getToken() }
    dp.host = this.host;
    dp.path = "user/schedule/"
    dp.method = "GET"
    dp.timeout = "10000"
    dp.generateRequest()
      .then(this._responseSuccess.bind(this))
      .catch(this._responseError.bind(this));
  }

  /**
   * Requiere autenticación. Envia a la API la hora, minutos y stationId.
   * @param {Object} param0 hora, minutos y stationId.
   */
  saveSchedule({ hour, minutes, stationId }) {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');
    
    let dp = new DP();
    dp.headers = {'Authorization': 'JWT '+ getToken() }
    dp.host = this.host;
    dp.path = "user/schedule/"
    dp.method = "POST"
    dp.body = { hour, minutes, stationId }
    dp.timeout = "10000"
    dp.generateRequest()
      .then(this._responseSuccess.bind(this))
      .catch(this._responseError.bind(this));
  }

  /**
   * Requiere autenticación. Envia a la API el nuevo recordatorio.
   * @param {Number} id ID de la estación.
   */
  deleteSchedule(id) {
    this._emmit('service_loaded', 'display-block');
    const DP = customElements.get('cells-generic-dp');
    
    let dp = new DP();
    dp.headers = {'Authorization': 'JWT '+ getToken() }
    dp.host = this.host;
    dp.path = "user/schedule/" + id
    dp.method = "DELETE"
    dp.timeout = "10000"
    dp.generateRequest()
      .then(this._responseSuccess.bind(this))
      .catch(this._responseError.bind(this));
  }

  /**
   * 
   * @param {String} eventName nombre del evento
   * @param {*} detail Detalle a  enviar.
   * Es un método de utilidad. 
   */
  _emmit(eventName, detail) {
    this.dispatchEvent(new CustomEvent(eventName, { detail, composed: true, bubbles: true }));
  }

  /**
   * Es el callback de la respuesta satisfactoria.
   * Emite eventos con el detalle y estado.
   * @param {Object} res la respuesta del servicio
   */
  _responseSuccess(res) {
    console.log('Retorna recordatorios :', res.response.data);
    this._emmit('schedule_success', res.response.data);
    this._emmit('service_loaded', 'fade-out');
  }

  /**
   * Es el callback de la respuesta de error.
   * Emite eventos con el detalle y estado.
   * Si no está autenticado, emite el mensaje correspondiente.
   * @param {Object} res la respuesta del servicio
   */
  _responseError(res) {
    if(res.status == 401 || res.status == 403) {
      return this._emmit('user_unauthorized', res);
    }
    this._emmit('message_response', res.response.message);
    this._emmit('show_message', true);
    this._emmit('service_loaded', 'fade-out');
  }
}

// Register the element with the browser
customElements.define(DmSchedule.is, DmSchedule);